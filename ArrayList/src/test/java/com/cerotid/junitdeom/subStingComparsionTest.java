package com.cerotid.junitdeom;

import org.junit.Assert;
import org.junit.Test;

import com.Cerotid.qaautomation.SubStringComparision;

public class subStingComparsionTest {

	@Test
	public void testSubString_1_success() throws Exception {
		SubStringComparision subStringComparision = new SubStringComparision();
		Assert.assertTrue(subStringComparision.isSubstring("Welcome to QA Class", "Welcome"));
	}
	
	@Test
	public void testSubString_2_failure() throws Exception {
		SubStringComparision subStringComparision = new SubStringComparision();
		Assert.assertFalse(subStringComparision.isSubstring("Welcome to QA Class", "Hello"));
	}
	
	@Test
	public void testSubString_3_success() throws Exception {
		SubStringComparision subStringComparision = new SubStringComparision();
		Assert.assertTrue(subStringComparision.isSubstring("Welcome to QA Class", "Clas"));
	}
}
