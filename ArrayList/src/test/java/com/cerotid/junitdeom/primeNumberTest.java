package com.cerotid.junitdeom;

import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import com.Cerotid.qaautomation.primeNumber;

public class primeNumberTest {

	primeNumber prime;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		//Instantiation ==> Object creation
		prime = new primeNumber();
		
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testprimenumber() {
		
		assertTrue(prime.primeNum(3)); 
		assertFalse(prime.primeNum(10));
		assertTrue(prime.primeNum(13));
		assertTrue(prime.primeNum(17));
		assertFalse(prime.primeNum(9));

}
	
}