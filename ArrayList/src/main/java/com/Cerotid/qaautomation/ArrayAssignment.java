package com.Cerotid.qaautomation;

import java.util.Scanner;

public class ArrayAssignment {

	public static void main(String[] args) {
		
		learningArrayconcept();

	}

	private static void learningArrayconcept() {
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the length");
		int length = s.nextInt();
		int[] myArray = new int[length];
		System.out.println("Enter the element of the array");

		for (int i = 0; i < length; i++) {
			myArray[i] = s.nextInt();
		}
		int highestNumber = myArray[0];

		System.out.println("Even number are:");
		int sum = 0;
		double mean;
		for (int j = 1; j <= myArray.length - 1; j++) {
			if (highestNumber < myArray[j]) {
				highestNumber = myArray[j];

			}
			if (myArray[j] % 2 == 0) {

				System.out.println(myArray[j]);
			}

			sum += myArray[j];

		}

		mean = sum / length;

		System.out.println("Highest Number is: " + highestNumber);
		System.out.println("Sum value: " + sum);
		System.out.println("Mean value:" + mean);

	}

}